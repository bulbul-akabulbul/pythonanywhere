
# A very simple Flask Hello World app for you to get started with...
from flask import Flask
from flask import render_template
from flask import request
import pusher
import datetime
import requests
import json
from html import unescape
import random
import string
from subprocess import call

ID_LENGTH = 10



pusher_client = pusher.Pusher(app_id='803347',
  key='764dea3cf1ca308e6aa0',
  secret='82dd0fd2978af000e802',
  cluster='eu',
  ssl=True)

currently_playing = False
current_time = 0
current_video = ""
last_time_updated = datetime.datetime.now()


app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello from Flask!'

@app.route("/pusher", methods=["GET"])
def return_pusher():
    global currently_playing
    global current_time
    global current_video
    global last_time_updated
    if request.args.get("videoid", '') != '':
        current_video = request.args.get("videoid", '')
        current_time = 0
        currently_playing = False
        last_time_updated = datetime.datetime.now()
        if currently_playing:
            current_time += (datetime.datetime.now() - last_time_updated).total_seconds() + 3 # accounting for buffering
            print(datetime.datetime.now())
            print(last_time_updated)
            print(current_time)
            print((datetime.datetime.now() - last_time_updated).total_seconds())
            last_time_updated = datetime.datetime.now()
            print(int(current_time))
    id = ''.join(random.choice(string.printable[:62]) for i in range(ID_LENGTH))
    return render_template("pusher.html", videoid=current_video, currentlyPlaying=currently_playing, currentTime=int(current_time), clientID=id)

@app.route("/alert/<string:action>", methods=["GET", "POST"])
def return_alert(action):
    global pusher_client
    global currently_playing
    global current_time
    global last_time_updated
    global current_video
    if request.method == "POST":
        print("got webhook")
        print(request.get_json()["events"][0])
        webhook = pusher_client.validate_webhook(key=request.headers.get('X-Pusher-Key'),
            signature=request.headers.get('X-Pusher-Signature'),
            body=request.data)
        for event in webhook['events']:
            if event['name'] == "member_added":
                print("User joined: %s" % event["user_id"])
            elif event['name'] == "member_removed":
                print("User left: %s" % event["user_id"])

    else:
        time = request.args.get('time',0,int)
        print(action)
        print("Action")
        if action == "play" or action == "pause":
            print("action is play or pause")
        elif action == "loadvideo":
            print("action is loadvideo")
            current_video = request.args.get('videoid','')
        pusher_client.trigger('my-channel', 'alert', {'origin': request.args.get('origin'), 'action': action, 'time': time, 'videoid': current_video})
        currently_playing = action == "play"
        current_time = time
        last_time_updated = datetime.datetime.now()
    return "Alerted"

@app.route("/search", methods=["GET"])
def return_search():
    r = requests.get("https://www.googleapis.com/youtube/v3/search",
        params={"part":"snippet", "q":request.args.get("query",''), "type":"video", "maxResults": 10 ,"key":"AIzaSyAHRaCBS09rXm15bDuaZC5SAyQ4F4agS7k"},
        headers={"Accept": "application/json", "Referer": "https://galsol.pythonanywhere.com"})
    j = json.loads(r.text)
    final = ""
    for item in j["items"]:
        print(item)
        title = unescape(item["snippet"]["title"])
        final += render_template("searchresult.html", videoid=item["id"]["videoId"], videoName=title[:20], videoNameFull=title, thumbnail=item["snippet"]["thumbnails"]["default"]["url"])
    return final

@app.route("/git", methods=["POST"])
def return_git():
    global username
    global password

    print("Updating git...")
    js = request.get_json()
    print(js["user_name"])
    if js["user_name"] == "bulbul-akabulbul" and request.headers.get("X-Gitlab-Token", '') == 'galsol12345':
        call("./gitpull")
        # Reloading
        my_domain = 'galsol.pythonanywhere.com'
        username = 'galsol'
        token = '8d061a3675066b6e6a79a12ff96627e57b84183b'
        response = requests.post('https://www.pythonanywhere.com/api/v0/user/{username}/webapps/{domain}/reload/'.format(username=username, domain=my_domain),
            headers={'Authorization': 'Token {token}'.format(token=token)})
        if response.status_code == 200:
            print('All OK')
        else:
            print('Got unexpected status code {}: {!r}'.format(response.status_code, response.content))
    return "OK"

@app.route("/pusher/auth", methods=['POST'])
def pusher_authentication():

  # pusher_client is obtained through pusher_client = pusher.Pusher( ...  )
  auth = pusher_client.authenticate(channel=request.form['channel_name'],
    socket_id=request.form['socket_id'],
    custom_data={
      u'user_id': u'1',
      u'user_info': {
        u'twitter': u'@pusher'
      }
    })
  return json.dumps(auth)






